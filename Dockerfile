ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG ANTS_VERSION=2.3.5
ARG PYTHON_VERSION=3.9.7

FROM registry.gitlab.com/neurobuilds/ants:${ANTS_VERSION}-${BASE_DISTRO}${BASE_VERSION} as ants
FROM registry.gitlab.com/neurobuilds/miniconda:${PYTHON_VERSION}-${BASE_DISTRO}${BASE_VERSION} as miniconda

FROM ${BASE_DISTRO}:${BASE_VERSION}

# Software Directory
LABEL maintainer=blake.dewey@jhu.edu

# Copy ANTs Build Artifacts
COPY --from=ants /opt/ants /opt/ants

# Export ANTs Environment Variables
ENV PATH /opt/ants/bin:${PATH}
ENV LD_LIBRARY_PATH /opt/ants/lib:${LD_LIBRARY_PATH}
ENV ANTSPATH /opt/ants/bin

# Copy python Build Artifacts
COPY --from=miniconda /opt/conda /opt/conda

# Update Environment Variables
ENV PATH /opt/conda/bin:${PATH}

# Install required packages
RUN conda install -c defaults -c conda-forge numpy scipy nibabel && \
    pip install nipype

# Get python scripts
ADD flairstar.py /opt

# Add volume mount point
VOLUME /data

ENTRYPOINT ["/opt/conda/bin/python", "/opt/flairstar.py"]
