import os
import shutil
import argparse

from nipype import Workflow, Node
from nipype.utils.filemanip import split_filename
from nipype.interfaces.base import BaseInterface, BaseInterfaceInputSpec, TraitedSpec, traits, File, isdefined
from nipype.interfaces.ants.base import ANTSCommand, ANTSCommandInputSpec
from nipype.interfaces.ants import Registration, N4BiasFieldCorrection
from nipype.interfaces.ants.resampling import ApplyTransforms, ApplyTransformsOutputSpec, ApplyTransformsInputSpec


class Check3DInputSpec(BaseInterfaceInputSpec):
    input_image = File(exists=True, desc='image to check', mandatory=True)


class Check3DOutputSpec(TraitedSpec):
    out_file = File(desc='3d image')


class Check3D(BaseInterface):
    input_spec = Check3DInputSpec
    output_spec = Check3DOutputSpec

    def _run_interface(self, runtime):
        import nibabel as nib
        
        obj = nib.load(self.inputs.input_image)
        if len(obj.shape) > 3:
            obj_list = nib.four_to_three(obj)
            obj_list[0].to_filename(split_filename(self.inputs.input_image)[1] + '_3d.nii.gz')
        else:
            obj.to_filename(split_filename(self.inputs.input_image)[1] + '_3d.nii.gz')
        
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['out_file'] = os.path.abspath(split_filename(self.inputs.input_image)[1] + '_3d.nii.gz')
        return outputs


class ReorientInputSpec(BaseInterfaceInputSpec):
    input_image = File(exists=True, desc='image to check', mandatory=True)
    orientation = traits.String('RAI', desc='orientation string', mandatory=True, usedefault=True)


class ReorientOutputSpec(TraitedSpec):
    out_file = File(desc='reoriented image')


class Reorient(BaseInterface):
    input_spec = ReorientInputSpec
    output_spec = ReorientOutputSpec
    
    def _run_interface(self, runtime):
        import nibabel as nib
        
        orient_dict = {'R': 'L', 'A': 'P', 'I': 'S', 'L': 'R', 'P': 'A', 'S': 'I'}
        
        obj = nib.load(self.inputs.input_image)
        target_orient = [orient_dict[char] for char in self.inputs.orientation]
        if nib.aff2axcodes(obj.affine) != tuple(target_orient):
            orig_ornt = nib.orientations.io_orientation(obj.affine)
            targ_ornt = nib.orientations.axcodes2ornt(target_orient)
            ornt_xfm = nib.orientations.ornt_transform(orig_ornt, targ_ornt)
            
            affine = obj.affine.dot(nib.orientations.inv_ornt_aff(ornt_xfm, obj.shape))
            data = nib.orientations.apply_orientation(obj.dataobj, ornt_xfm)
            obj_new = nib.Nifti1Image(data, affine, obj.header)
            obj_new.to_filename(split_filename(self.inputs.input_image)[1] + '_reorient.nii.gz')
        else:
            obj.to_filename(split_filename(self.inputs.input_image)[1] + '_reorient.nii.gz')
        
        return runtime
    
    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['out_file'] = os.path.abspath(split_filename(self.inputs.input_image)[1] + '_reorient.nii.gz')
        return outputs


class ResampleImageBySpacingInputSpec(ANTSCommandInputSpec):
    dimension = traits.Enum(3, 2, 4, desc='image dimension', argstr='%d', position=0, usedefault=True)
    in_file = File(exists=True, desc='file to be resampled', argstr='%s', position=1, mandatory=True)
    out_file = File(desc='resampled file', argstr='%s', position=2, genfile=True, hash_files=False)
    x_spacing = traits.Float(desc='x dimension spacing', argstr='%g', position=3, mandatory=True)
    y_spacing = traits.Float(desc='y dimension spacing', argstr='%g', position=4, mandatory=True)
    z_spacing = traits.Float(desc='z dimension spacing', argstr='%g', position=5, mandatory=True)
    smooth_image = traits.Int(False, desc='smooth image?', argstr='%d', position=6, usedefault=True)


class ResampleImageBySpacingOutputSpec(TraitedSpec):
    out_file = File(desc='resampled file')


class ResampleImageBySpacing(ANTSCommand):
    input_spec = ResampleImageBySpacingInputSpec
    output_spec = ResampleImageBySpacingOutputSpec
    _cmd = 'ResampleImageBySpacing'

    def _gen_filename(self, name):
        if name == 'out_file':
            return self.inputs.out_file if isdefined(self.inputs.out_file) \
                else split_filename(self.inputs.in_file)[1] + '_resampled.nii.gz'
        return None

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['out_file'] = os.path.abspath(self.inputs.out_file if isdefined(self.inputs.out_file)
                                              else split_filename(self.inputs.in_file)[1] + '_resampled.nii.gz')
        return outputs


class ConvertTransformFileInputSpec(ANTSCommandInputSpec):
    dimension = traits.Enum(3, 2, desc='transform dimension', argstr='%d', position=0, usedefault=True)
    input_transform = File(exists=True, desc='input (binary) transform file', argstr='%s', position=1, mandatory=True)
    output_transform = File(desc='output text transform file', argstr='%s', position=2, name_source=['input_transform'],
                            name_template='%s_matrix.tfm', hash_files=False)
    extract_matrix = traits.Bool(desc='extract the matrix only', argstr='-m', position=3,
                                 xor_inputs=['homogeneous_matrix', 'convert_to_affine'])
    homogeneous_matrix = traits.Bool(desc='extract a homogeneous matrix only', argstr='--hm', position=3,
                                     xor_inputs=['extract_matrix', 'convert_to_affine'])
    convert_to_affine = traits.Bool(desc='convert to an AffineTransform', argstr='--convertToAffineType', position=3,
                                    xor_inputs=['extract_matrix', 'homogeneous_matrix'])


class ConvertTransformFileOutputSpec(TraitedSpec):
    output_transform = File(exists=True, desc='output text transform file')


class ConvertTransformFile(ANTSCommand):
    input_spec = ConvertTransformFileInputSpec
    output_spec = ConvertTransformFileOutputSpec
    _cmd = 'ConvertTransformFile'

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['output_transform'] = os.path.abspath(self.inputs.output_transform) \
            if isdefined(self.inputs.output_transform) else \
            os.path.abspath(split_filename(self.inputs.input_transform)[1] + '_matrix.tfm')
        return outputs


class AIInputSpec(ANTSCommandInputSpec):
    dimension = traits.Enum(
        3, 2, usedefault=True, argstr="-d %d", desc="dimension of output image"
    )
    verbose = traits.Bool(
        False, usedefault=True, argstr="-v %d", desc="enable verbosity"
    )

    fixed_image = File(
        exists=True,
        mandatory=True,
        desc="Image to which the moving_image should be transformed",
    )
    moving_image = File(
        exists=True,
        mandatory=True,
        desc="Image that will be transformed to fixed_image",
    )

    fixed_image_mask = File(exists=True, argstr="-x %s", desc="fixed mage mask")
    moving_image_mask = File(
        exists=True, requires=["fixed_image_mask"], desc="moving mage mask"
    )

    metric_trait = (
        traits.Enum("Mattes", "GC", "MI"),
        traits.Int(32),
        traits.Enum("Regular", "Random", "None"),
        traits.Range(value=0.2, low=0.0, high=1.0),
    )
    metric = traits.Tuple(
        *metric_trait, argstr="-m %s", mandatory=True, desc="the metric(s) to use."
    )

    transform = traits.Tuple(
        traits.Enum("Affine", "Rigid", "Similarity"),
        traits.Range(value=0.1, low=0.0, exclude_low=True),
        argstr="-t %s[%g]",
        usedefault=True,
        desc="Several transform options are available",
    )

    principal_axes = traits.Bool(
        False,
        usedefault=True,
        argstr="-p %d",
        xor=["blobs"],
        desc="align using principal axes",
    )
    search_factor = traits.Tuple(
        traits.Float(20),
        traits.Range(value=0.12, low=0.0, high=1.0),
        usedefault=True,
        argstr="-s [%g,%g]",
        desc="search factor",
    )

    search_grid = traits.Either(
        traits.Tuple(
            traits.Float, traits.Tuple(traits.Float, traits.Float, traits.Float)
        ),
        traits.Tuple(traits.Float, traits.Tuple(traits.Float, traits.Float)),
        argstr="-g %s",
        desc="Translation search grid in mm",
        min_ver="2.3.0",
    )

    convergence = traits.Tuple(
        traits.Range(low=1, high=10000, value=10),
        traits.Float(1e-6),
        traits.Range(low=1, high=100, value=10),
        usedefault=True,
        argstr="-c [%d,%g,%d]",
        desc="convergence",
    )

    output_transform = File(
        "initialization.mat", usedefault=True, argstr="-o %s", desc="output file name"
    )


class AIOuputSpec(TraitedSpec):
    output_transform = File(exists=True, desc="output file name")


class AI(ANTSCommand):
    """
    Calculate the optimal linear transform parameters for aligning two images.

    Examples
    --------
    >>> AI(
    ...     fixed_image='structural.nii',
    ...     moving_image='epi.nii',
    ...     metric=('Mattes', 32, 'Regular', 1),
    ... ).cmdline
    'antsAI -c [10,1e-06,10] -d 3 -m Mattes[structural.nii,epi.nii,32,Regular,1]
    -o initialization.mat -p 0 -s [20,0.12] -t Affine[0.1] -v 0'

    >>> AI(fixed_image='structural.nii',
    ...    moving_image='epi.nii',
    ...    metric=('Mattes', 32, 'Regular', 1),
    ...    search_grid=(12, (1, 1, 1)),
    ... ).cmdline
    'antsAI -c [10,1e-06,10] -d 3 -m Mattes[structural.nii,epi.nii,32,Regular,1]
    -o initialization.mat -p 0 -s [20,0.12] -g [12.0,1x1x1] -t Affine[0.1] -v 0'

    """

    _cmd = "antsAI"
    input_spec = AIInputSpec
    output_spec = AIOuputSpec

    def _run_interface(self, runtime, correct_return_codes=(0,)):
        runtime = super(AI, self)._run_interface(runtime, correct_return_codes)

        self._output = {
            "output_transform": os.path.join(
                runtime.cwd, os.path.basename(self.inputs.output_transform)
            )
        }
        return runtime

    def _format_arg(self, opt, spec, val):
        if opt == "metric":
            val = "%s[{fixed_image},{moving_image},%d,%s,%g]" % val
            val = val.format(
                fixed_image=self.inputs.fixed_image,
                moving_image=self.inputs.moving_image,
            )
            return spec.argstr % val

        if opt == "search_grid":
            fmtval = "[%s,%s]" % (val[0], "x".join("%g" % v for v in val[1]))
            return spec.argstr % fmtval

        if opt == "fixed_image_mask":
            if isdefined(self.inputs.moving_image_mask):
                return spec.argstr % ("[%s,%s]" % (val, self.inputs.moving_image_mask))

        return super(AI, self)._format_arg(opt, spec, val)

    def _list_outputs(self):
        return getattr(self, "_output")


def write_transform(trans_mtx, transform_file):
    import os
    import numpy as np
    trans_mtx = np.array(trans_mtx)
    trans = list(trans_mtx[0, :3]) + list(trans_mtx[1, :3]) + list(trans_mtx[2, :3]) + list(trans_mtx[:3, 3])
    with open(transform_file, 'w') as trans_file:
        trans_file.write('#Insight Transform File V1.0\n')
        trans_file.write('#Transform 0\n')
        trans_file.write('Transform: AffineTransform_double_3_3\n')
        trans_file.write('Parameters: ' + ' '.join(['%f' % item for item in trans]) + '\n')
        trans_file.write('FixedParameters: 0 0 0\n')
    return os.path.abspath(transform_file)


class QuickAlignImagesInputSpec(BaseInterfaceInputSpec):
    moving_image = File(exists=True, mandatory=True,
                        desc='image to apply transformation to (generally a coregistered functional)')
    fixed_image = File(exists=True, mandatory=True,
                       desc='image to apply transformation to (generally a coregistered functional)')


class QuickAlignImagesOutputSpec(TraitedSpec):
    rigid_transform = File(exists=True, desc='output forward rigid matrix file')


class QuickAlignImages(BaseInterface):
    """
    Runs a quick affine (or rigid) alignment algorithm based on antsAI. THe moving and fixed images are
    subsampled to 4x4x4mm and antsAI is run.

    Examples:
    ---------

    >>> from iacl_pipeline.interfaces.registration import QuickAlignImages

    >>> qai = QuickAlignImages()
    >>> qai.inputs.moving_image = 't1.nii.gz'
    >>> qai.inputs.fixed_image = 't1_atlas.nii.gz'
    >>> qai.inputs.extract_inv_scale = True
    >>> qai.run() #doctest: +SKIP
    """
    input_spec = QuickAlignImagesInputSpec
    output_spec = QuickAlignImagesOutputSpec

    def _run_interface(self, runtime):
        import numpy as np

        in_result = ResampleImageBySpacing(in_file=self.inputs.moving_image, x_spacing=4, y_spacing=4, z_spacing=4,
                                           smooth_image=True, terminal_output='none').run()
        ref_result = ResampleImageBySpacing(in_file=self.inputs.fixed_image, x_spacing=4, y_spacing=4, z_spacing=4,
                                            smooth_image=True, terminal_output='none').run()
        init_result = AI(dimension=3, moving_image=in_result.outputs.out_file, fixed_image=ref_result.outputs.out_file,
                         metric=('Mattes', 32, 'Regular', 0.2),
                         transform=('Rigid', 0.1),
                         search_factor=(20, 0.12), convergence=(10, 1e-6, 10)).run()
        os.rename(init_result.outputs.output_transform, split_filename(self.inputs.moving_image)[1] + '_rigid.mat')

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['rigid_transform'] = os.path.abspath(split_filename(self.inputs.moving_image)[1] + '_rigid.mat')
        return outputs


class TransformImageInputSpec(ApplyTransformsInputSpec):
    clip_to_input_range = traits.Bool(desc='clip output image to intensity range of the input image')
    use_image_min = traits.Bool(desc='use the minimum intensity of the input image as the default value for output')


class TransformImage(ApplyTransforms):
    input_spec = TransformImageInputSpec
    output_spec = ApplyTransformsOutputSpec

    def _format_arg(self, opt, spec, val):
        if opt == 'default_value' and self.inputs.use_image_min:
            import nibabel as nib
            import numpy as np
            val = float(nib.load(self.inputs.input_image).get_fdata().astype(np.float32).min())
        return super(TransformImage, self)._format_arg(opt, spec, val)

    def _run_interface(self, runtime, correct_return_codes=(0,)):
        import nibabel as nib

        runtime = super(TransformImage, self)._run_interface(runtime, correct_return_codes)
        outputs = super(TransformImage, self)._list_outputs()
        if self.inputs.clip_to_input_range:
            img_obj = nib.load(outputs['output_image'])
            input_data = nib.load(self.inputs.input_image).get_fdata()
            img_data = img_obj.get_fdata()
            img_data[img_data < input_data.min()] = input_data.min()
            img_data[img_data > input_data.max()] = input_data.max()
            img_obj = nib.Nifti1Image(img_data, None, img_obj.header)
            img_obj.to_filename(outputs['output_image'])

        return runtime


class ImageCalcInputSpec(BaseInterfaceInputSpec):
    image1 = File(exists=True, desc='first image volume', mandatory=True)
    image2 = File(exists=True, desc='second mask volume', mandatory=True)
    function = traits.Enum('multiply', 'divide', 'add', 'subtract', desc='function to be performed', mandatory=True)


class ImageCalcOutputSpec(TraitedSpec):
    output_image = File(exists=True, desc='calculated image volume')


class ImageCalc(BaseInterface):
    input_spec = ImageCalcInputSpec
    output_spec = ImageCalcOutputSpec

    def _run_interface(self, runtime):
        import nibabel as nib
        import numpy as np

        image1_obj = nib.load(self.inputs.image1)
        image1_data = image1_obj.get_fdata().astype(np.float32)
        image2_data = nib.load(self.inputs.image2).get_fdata().astype(np.float32)

        final_image_data = np.zeros_like(image1_data)
        if self.inputs.function == 'multiply':
            final_image_data = image1_data * image2_data
        elif self.inputs.function == 'divide':
            final_image_data = np.nan_to_num(image1_data / image2_data)
        elif self.inputs.function == 'add':
            final_image_data = image1_data + image2_data
        elif self.inputs.function == 'subtract':
            final_image_data = image1_data - image2_data
        final_image = nib.Nifti1Image(final_image_data, None, image1_obj.header)
        final_image.set_data_dtype(np.float32)
        final_image.to_filename(split_filename(self.inputs.image1)[1] + '_' + self.inputs.function + '.nii.gz')
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['output_image'] = os.path.abspath(split_filename(self.inputs.image1)[1] + '_' + self.inputs.function +
                                                  '.nii.gz')
        return outputs
    
    
class MoveResultFileInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='input file to be renamed', mandatory=True)
    output_name = traits.String(desc='output name string')


class MoveResultFileOutputSpec(TraitedSpec):
    out_file = File(desc='path of moved file')


class MoveResultFile(BaseInterface):
    input_spec = MoveResultFileInputSpec
    output_spec = MoveResultFileOutputSpec

    def _run_interface(self, runtime):
        shutil.copyfile(self.inputs.in_file, self.inputs.output_name)
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['out_file'] = self.inputs.output_name
        return outputs

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--flair', type=str, required=True)
    parser.add_argument('-t', '--t2star', type=str, required=True)
    parser.add_argument('-o', '--output-filename', type=str, required=True)
    parser.add_argument('--orientation', type=str, default='RAI')
    parser.add_argument('--threads', type=int, default=1)
    args = parser.parse_args()
    
    for argname in ['flair', 't2star', 'output_filename']:
        setattr(args, argname, os.path.join('/data', getattr(args, argname)))
    
    # Initialize workflow
    wf = Workflow('flairstar')
    
    # Check 3D
    check_3d_flair = Node(Check3D(), 'check_3d_flair')
    check_3d_flair.inputs.input_image = args.flair

    check_3d_t2star = Node(Check3D(), 'check_3d_t2star')
    check_3d_t2star.inputs.input_image = args.t2star
    
    # Reorient images
    reorient_flair = Node(Reorient(), 'reorient_flair')
    reorient_flair.orientation = args.orientation.upper()
    wf.connect(check_3d_flair, 'out_file', reorient_flair, 'input_image')

    reorient_t2star = Node(Reorient(), 'reorient_t2star')
    reorient_t2star.orientation = args.orientation.upper()
    wf.connect(check_3d_t2star, 'out_file', reorient_t2star, 'input_image')

    # Use N4BiasFieldCorrection on all FLAIR and T2Star images
    flair_n4_correction = Node(N4BiasFieldCorrection(), 'flair_n4_correction')
    flair_n4_correction.inputs.shrink_factor = 4
    flair_n4_correction.inputs.n_iterations = [200, 200, 200, 200]
    flair_n4_correction.inputs.convergence_threshold = 0.0005
    flair_n4_correction.inputs.num_threads = args.threads
    wf.connect(reorient_flair, 'out_file', flair_n4_correction, 'input_image')

    t2star_n4_correction = Node(N4BiasFieldCorrection(), 't2star_n4_correction')
    t2star_n4_correction.inputs.shrink_factor = 4
    t2star_n4_correction.inputs.n_iterations = [200, 200, 200, 200]
    t2star_n4_correction.inputs.convergence_threshold = 0.0005
    t2star_n4_correction.inputs.num_threads = args.threads
    wf.connect(reorient_t2star, 'out_file', t2star_n4_correction, 'input_image')

    # Register scan images to scan T1 (or baseline T1 if missing)
    init_align_images = Node(QuickAlignImages(), 'init_align_images')
    wf.connect([(flair_n4_correction, init_align_images, [('output_image', 'moving_image')]),
                (t2star_n4_correction, init_align_images, [('output_image', 'fixed_image')])])
    
    image_reg_coarse = Node(Registration(), 'image_reg_coarse')
    image_reg_coarse.inputs.num_threads = args.threads
    image_reg_coarse.inputs.metric = ['MI']
    image_reg_coarse.inputs.metric_weight = [1.0]
    image_reg_coarse.inputs.radius_or_number_of_bins = [32]
    image_reg_coarse.inputs.sampling_strategy = ['Regular']
    image_reg_coarse.inputs.sampling_percentage = [0.1]
    image_reg_coarse.inputs.transforms = ['Rigid']
    image_reg_coarse.inputs.transform_parameters = [(0.1,)]
    image_reg_coarse.inputs.number_of_iterations = [[1000, 500, 250, 125]]
    image_reg_coarse.inputs.convergence_threshold = [1.e-6]
    image_reg_coarse.inputs.convergence_window_size = [10]
    image_reg_coarse.inputs.smoothing_sigmas = [[4, 3, 2, 1]]
    image_reg_coarse.inputs.sigma_units = ['vox']
    image_reg_coarse.inputs.shrink_factors = [[16, 8, 4, 2]]
    image_reg_coarse.inputs.winsorize_upper_quantile = 0.99
    image_reg_coarse.inputs.winsorize_lower_quantile = 0.01
    image_reg_coarse.inputs.collapse_output_transforms = True  # For explicit completeness
    image_reg_coarse.inputs.use_histogram_matching = True
    image_reg_coarse.inputs.float = True
    image_reg_coarse.inputs.use_estimate_learning_rate_once = [False]
    image_reg_coarse.inputs.write_composite_transform = True  # Output a single file to use in the fine step
    wf.connect([(flair_n4_correction, image_reg_coarse, [('output_image', 'moving_image')]),
                (t2star_n4_correction, image_reg_coarse, [('output_image', 'fixed_image')]),
                (init_align_images, image_reg_coarse, [('rigid_transform', 'initial_moving_transform')])])
    image_reg_coarse.inputs.invert_initial_moving_transform = False  # Must be here to avoid warning

    image_reg_fine = Node(Registration(), 'image_reg_fine')
    image_reg_fine.inputs.num_threads = args.threads
    image_reg_fine.inputs.metric = ['MI']
    image_reg_fine.inputs.metric_weight = [1.0]
    image_reg_fine.inputs.radius_or_number_of_bins = [32]
    image_reg_fine.inputs.sampling_strategy = ['Regular']
    image_reg_fine.inputs.sampling_percentage = [0.1]
    image_reg_fine.inputs.transforms = ['Rigid']
    image_reg_fine.inputs.transform_parameters = [(0.1,)]
    image_reg_fine.inputs.number_of_iterations = [[1000, 500, 250, 125]]
    image_reg_fine.inputs.convergence_threshold = [1e-6]
    image_reg_fine.inputs.convergence_window_size = [10]
    image_reg_fine.inputs.smoothing_sigmas = [[3, 2, 1, 0]]
    image_reg_fine.inputs.sigma_units = ['vox']
    image_reg_fine.inputs.shrink_factors = [[8, 4, 2, 1]]
    image_reg_fine.inputs.winsorize_upper_quantile = 0.99
    image_reg_fine.inputs.winsorize_lower_quantile = 0.01
    image_reg_fine.inputs.collapse_output_transforms = True  # For explicit completeness
    image_reg_fine.inputs.use_histogram_matching = True
    image_reg_fine.inputs.float = True
    image_reg_fine.inputs.use_estimate_learning_rate_once = [True]
    wf.connect([(flair_n4_correction, image_reg_fine, [('output_image', 'moving_image')]),
                (t2star_n4_correction, image_reg_fine, [('output_image', 'fixed_image')]),
                (image_reg_coarse, image_reg_fine, [('composite_transform', 'initial_moving_transform')])])
    image_reg_fine.inputs.invert_initial_moving_transform = False  # Must be here to avoid warning

    transform_flair = Node(TransformImage(), 'transform_flair')
    transform_flair.inputs.num_threads = args.threads
    transform_flair.inputs.interpolation = 'BSpline'
    transform_flair.inputs.interpolation_parameters = (3,)  # Order for BSpline (Cubic)
    transform_flair.inputs.clip_to_input_range = True
    transform_flair.inputs.use_image_min = True
    transform_flair.inputs.float = True
    wf.connect([(flair_n4_correction, transform_flair, [('output_image', 'input_image')]),
                (t2star_n4_correction, transform_flair, [('output_image', 'reference_image')]),
                (image_reg_fine, transform_flair, [('forward_transforms', 'transforms')])])
    
    multiply_images = Node(ImageCalc(function='multiply'), 'multiply_images')
    wf.connect([(transform_flair, multiply_images, [('output_image', 'image1')]),
                (t2star_n4_correction, multiply_images, [('output_image', 'image2')])])

    move_flairstar = Node(MoveResultFile(), 'move_flairstar')
    move_flairstar.inputs.output_name = args.output_filename
    wf.connect(multiply_images, 'output_image', move_flairstar, 'in_file')
    
    wf.run()
