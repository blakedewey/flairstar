This repository contains a simple script for the creation of FLAIR\* images from FLAIR and T2\*-weighed images.


## Installation/Usage
 
For simplicity, I will give instructions for Linux/macOS systems. These instructions should be very similar for Windows systems (except for perhaps the filepaths).
 
1. Download and install Docker from https://docs.docker.com/get-docker/
2. Copy your DICOM images for the FLAIR and T2\* sequences to your processing computer.
3. Open your Terminal and run: `docker run --rm -v /path/to/dicoms:/input -v /path/for/nifti/output:/output registry.gitlab.com/blakedewey/dcm2niix-app`. The `/path/to/dicoms` should be replaced with the full path to the directory containing your dicom images. The `/path/for/nifti/output` should be replaced with the full path to the directory you would like the NIFTI images saved to.
4. After you have converted your DICOM files, move the FLAIR and T2\* NIFTI files to the same directory (if they aren’t already) and run `docker run --rm -v /path/to/data:/data registry.gitlab.com/blakedewey/flairstar -f FLAIR_FILENAME.nii.gz -t T2STAR_FILENAME.nii.gz -o OUTPUT_FILENAME.nii.gz`. Replace `/path/to/data` with the full path to the directory containing your NIFTI files. Replace `FLAIR_FILENAME.nii.gz` with the filename of the FLAIR NIFTI file and replace `T2STAR_FILENAME.nii.gz` with the filename of the T2\* NIFTI file. You can replace the `OUTPUT_FILENAME.nii.gz` with any filename (make sure to end it in .nii.gz).

* You can also specify `--threads N` at the end of the command, where `N` is an integer greater than 1 and less than the total number of cores on your machine. This will instruct the container to run parallel code for each processing step which will allow faster computation, but take up more of the computer's resources.
* If you would like the result images to be in a different orientation than axial, add `--orient XYZ` where X, Y, and Z are the directions for each of x-, y- and z-axis. For example, axial/transverse would be `RAI`, sagittal would be `ASR` and coronal would be `RSA`.

The output FLAIR\* image will be saved to the same directory as the input NIFTIs. It can be opened and viewed using MIPAV (https://mipav.cit.nih.gov/) or with FSLView.
